package com.running.utils;

import java.text.DecimalFormat;

public class TimeConverter {
	
	public static DecimalFormat timeFormatter = new DecimalFormat("00");
	
	/**
	 * @param minutes
	 * @return string as 00:00:00
	 */
	public static String convertMinsToTimeStr(float minutes) {
		StringBuffer timeStr = new StringBuffer();
		
		
		//less than an hour
		if(minutes/60 < 1){
			timeStr.append("00:");
			timeStr.append(timeFormatter.format((int)Math.floor(minutes)));
			timeStr.append(":");
			getSeconds(minutes, timeStr);
		} else {
			//hours
			timeStr.append(timeFormatter.format((int)Math.floor(minutes/60)));
			timeStr.append(":");
			//minutes
			float decimalMinutes = (float) ((Math.abs(Math.floor(minutes/60)-(minutes/60)))*60);
			timeStr.append(timeFormatter.format((int)decimalMinutes));
			timeStr.append(":");
			getSeconds(decimalMinutes, timeStr);
		}
		return timeStr.toString();
	}

	private static void getSeconds(float minutes, StringBuffer timeStr) {
		float decimalSeconds = Math.abs((float)Math.floor(minutes)- minutes);
		timeStr.append(timeFormatter.format((int)Math.round(decimalSeconds*60)));
	}

	 public static void main(String args[]) {
		String time = TimeConverter.convertMinsToTimeStr(12.5f);
		System.out.println("----time: " + time);
	}
}
