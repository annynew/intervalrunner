package com.running.model;

import java.util.Date;

import com.running.db.DBPlaylistAdapter;

import android.database.Cursor;

/**
 * Playlist value object
 * 
 * @author amouradi
 *
 */
public class Playlist extends BaseModel {

    private String name;
    private String description;
    private boolean isFast;
    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public boolean isFast() {
		return isFast;
	}
    
    /**
     * utility method for db updates
     * @return int, 1 for true, 0 for false
     */
    public int getFastInt(){
    	int fastInt = (isFast)?1:0;
    	return fastInt;
    }
    
	public void setFast(boolean isFast) {
		this.isFast = isFast;
	}
	
	public static Playlist populateFirstRecord(Cursor cursor) {
        Playlist playlist = new Playlist();
        boolean hasValues = cursor.moveToFirst();   
        if(!hasValues) {
            return playlist;
        }
        populateCurrent(cursor, playlist);
        return playlist;
    }
    
    
    
    public static void populateCurrent(Cursor cursor, Playlist playlist) {
        playlist.setId(cursor.getInt(cursor.getColumnIndex(DBPlaylistAdapter.ID)));
        playlist.setName(cursor.getString(cursor.getColumnIndex(DBPlaylistAdapter.NAME)));
        playlist.setDescription(cursor.getString(cursor.getColumnIndex(DBPlaylistAdapter.PLAYLIST_DESCRIPTION)));
        playlist.setCreateDate(new Date(cursor.getInt(cursor.getColumnIndex(DBPlaylistAdapter.CREATE_DATE))));
        playlist.setUpdateDate(new Date(cursor.getInt(cursor.getColumnIndex(DBPlaylistAdapter.UPDATE_DATE))));
        boolean convertFastFlag = (cursor.getInt(cursor.getColumnIndex(DBPlaylistAdapter.IS_FAST)) == 1)?true:false;
        playlist.setFast(convertFastFlag);
    }
    
}
