package com.running.model;

/**
 * represents List view item, where title is item's name and current value is
 * displayed under it
 * 
 * @author amouradi
 * 
 */
public class ListItem implements Item{

	private String title;
	private String bottomText;
	private String id;
	private String color;

	
	public ListItem(String id, String title, String bottomText) {
		this.id = id;
		this.bottomText = bottomText;
		this.title = title;
	}

	public ListItem(String title, String currentValue) {
		this.bottomText = currentValue;
		this.title = title;
	}

	public ListItem(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBottomText() {
		return bottomText;
	}

	public void setBottomText(String bottomText) {
		this.bottomText = bottomText;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String toString() {
		
	return "id=" + id + "; title=" + title + "; bottomText=" + bottomText + "; color:" + color;	
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
