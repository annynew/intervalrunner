package com.running.model;

/**
 * one lap
 * @author amourad
 *
 */
public class Lap {
	
	String title;
	Float lapMinutes;
	Float lapMiles;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Float getLapMinutes() {
		return lapMinutes;
	}
	public void setLapMinutes(Float lapMinutes) {
		this.lapMinutes = lapMinutes;
	}
	public Float getLapMiles() {
		return lapMiles;
	}
	public void setLapMiles(Float lapMiles) {
		this.lapMiles = lapMiles;
	}
	
	
	public String toString(){
		return "title: " + title + "|lapMinutes: " + lapMinutes + "| lapMiles" + lapMiles;
	}
	
	
}
