package com.running.model;

public interface Item {

	public String getTitle();
	public String getBottomText();
	public String getId();
}
