package com.running.model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author amourad
 * one run with all intervals calculated
 */
public class Run {

	private List<Lap> listOfLaps = new LinkedList<Lap>();
	private float totalTime;

	public List<Lap> getListOfLaps() {
		return listOfLaps;
	}

	public void setListOfLaps(List<Lap> listOfLaps) {
		this.listOfLaps = listOfLaps;
	}

    public void setTotalTime(float totalTime) {
        this.totalTime = totalTime;
    }

    
    /**
     * @return time in minutes
     */
    public float getTotalTime() {
        return totalTime;
    }
	
	
	
	
}
