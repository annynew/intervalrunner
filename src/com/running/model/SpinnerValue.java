package com.running.model;

/**
 * value object wil hold id and value for each drop down element
 * @author amouradi
 *
 */
public class SpinnerValue {
	private Integer id;
	private String name;
	
	public SpinnerValue(){}
	
	public SpinnerValue(Integer id, String name){
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return getId() + ":" + getName();
	}
	

}
