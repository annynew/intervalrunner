package com.running.model;

public class FileItem implements Comparable<FileItem>, Item {
	private String id;
	private String title;
	private int duration;
	private String composer;
	private String mediaId;

	public FileItem(String id, String title, String composer, int duration, String mediaId) {
		this.id = id;
		this.title = title;
		this.duration = duration;
		this.composer = composer;
		this.mediaId = mediaId;
	}
	
	@Override
	public String getId(){
		return id;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getBottomText() {
		StringBuffer str = new StringBuffer();
		if(composer != null && composer.length() > 0 ) {
			str.append(composer);
			str.append(". ");
		}
		str.append("Duration: "); 
		str.append(duration);
		return str.toString();
	}

	public int getDuration() {
		return duration;
	}

	public String getComposer() {
		return composer;
	}
	
	public String getMediaId() {
		return mediaId;
	}

	@Override
	public int compareTo(FileItem o) {
		if (this.title != null)
			return this.title.toLowerCase()
					.compareTo(o.getTitle().toLowerCase());
		else
			throw new IllegalArgumentException();
	}
}
