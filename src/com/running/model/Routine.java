package com.running.model;

import java.util.Date;

import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;

import android.database.Cursor;

/**
 * value object, represents Routine
 * @author amouradi
 *
 */
public class Routine extends BaseModel{
	private String name;
	private Float warmupTime;
	private Float regularPace;
	private Float totalDistance;
	private Float sprintDistance;
	private Float sprintPace;
	private Float recoveryTime;
	private Long totalSprints;
	private Boolean isMetric;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(Float totalDistance) {
		this.totalDistance = totalDistance;
	}
	
	public Float getSprintDistance() {
		return sprintDistance;
	}
	public void setSprintDistance(Float spintDistance) {
		this.sprintDistance = spintDistance;
	}
	
	/**
	 * min/per mile. So contains min
	 * @return
	 */
	public Float getSprintPace() {
		return sprintPace;
	}
	public void setSprintPace(Float sprintPace) {
		this.sprintPace = sprintPace;
	}
	
	
	/**
	 * how long to go for with a normal pace, in mins
	 * @return
	 */
	public Float getRecoveryTime() {
		return recoveryTime;
	}
	public void setRecoveryTime(Float recoveryTime) {
		this.recoveryTime = recoveryTime;
	}
	public Long getTotalSprints() {
		return totalSprints;
	}
	public void setTotalSprints(Long totalSprints) {
		this.totalSprints = totalSprints;
	}
	public Boolean isMetric() {
		return isMetric;
	}
	public void setMetric(Boolean isMetric) {
		this.isMetric = isMetric;
	}
	
	public static Routine populateFirstRecord(Cursor cursor) {
		Routine routine = new Routine();
		boolean hasValues = cursor.moveToFirst();	
		if(!hasValues) {
			return routine;
		}
		populateCurrent(cursor, routine);
		
		return routine;
	}
	
	public Float getWarmupTime() {
		return warmupTime;
	}
	public void setWarmupTime(Float warmupTime) {
		this.warmupTime = warmupTime;
	}
	public Float getRegularPace() {
		return regularPace;
	}
	public void setRegularPace(Float regularPace) {
		this.regularPace = regularPace;
	}
	
	public static void populateCurrent(Cursor cursor, Routine routine) {
		routine.setId(cursor.getInt(cursor.getColumnIndex(DBAdapterBase.ID)));
		routine.setName(cursor.getString(cursor.getColumnIndex(DBAdapterBase.NAME)));
		routine.setCreateDate(new Date(cursor.getInt(cursor.getColumnIndex(DBAdapterBase.CREATE_DATE))));
		routine.setUpdateDate(new Date(cursor.getInt(cursor.getColumnIndex(DBAdapterBase.UPDATE_DATE))));
		boolean isMetric = (cursor.getInt(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_IS_METRIC)) == 1)?true:false;
		routine.setMetric(isMetric);
		
		routine.setWarmupTime(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_WARMUP_TIME)));
		routine.setRegularPace(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_REGULAR_PACE)));
		
		routine.setRecoveryTime(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_RECOVERY_TIME)));
		routine.setSprintDistance(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_SPRINT_DISTANCE)));
		routine.setSprintPace(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_SPRINT_PACE)));
		routine.setTotalDistance(cursor.getFloat(cursor.getColumnIndex(DBRoutineAdapter.ROUTINE_TOTAL_DISTANCE)));
	}
	

}
