package com.running.model;

import java.util.Date;

/**
 * Base class for value objects,that get populated with db objects
 * 
 * @author amouradi
 *
 */
public class BaseModel {

    private Date createDate;
    private Date updateDate;
    private long id;

    public Date getCreateDate() {
    	return createDate;
    }

    public void setCreateDate(Date createDate) {
    	this.createDate = createDate;
    }

    public Date getUpdateDate() {
    	return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
    	this.updateDate = updateDate;
    }

    public long getId() {
    	return id;
    }

    public void setId(long id) {
    	this.id = id;
    }

}
