package com.running.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.running.db.DBAdapterBase.DatabaseHelper;
import com.running.model.ListItem;
import com.running.model.Playlist;
import com.running.model.SpinnerValue;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

/**
 * DB class representing playlist
 * @author amouradi
 *
 */
public class DBPlaylistAdapter extends DBAdapterBase {

 // define the layout of our table in fields
    // "_id" is used by Android for Content Providers and should
    // generally be an auto-incrementing key in every table.
    public static final String PLAYLIST_DESCRIPTION = "description";
    
    public static final String DB_PLAYLISTS_TABLE = "playlists";
    private static final String NEW_PLAYLIST_NAME = "Playlist ";
    //integer, fast track=1(yes), slow track=0(no)
    public static final String IS_FAST = "fast";
    
    public DBPlaylistAdapter(Context c) {  
        super(c);
    }

    @Override
    public DBAdapterBase open() throws SQLException {
        
        this.helper = new DatabaseHelper(context, DB_PLAYLISTS_TABLE, DB_PLAYLIST_CREATE);
        db = this.helper.getWritableDatabase();
        return this;
    }
    
    
    // a SQL statement to create a new playlist table
    private static final String DB_PLAYLIST_CREATE = "CREATE TABLE IF NOT EXISTS "
            + DB_PLAYLISTS_TABLE + " (" + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
            + NAME + " TEXT NOT NULL, " 
            + PLAYLIST_DESCRIPTION + " TEXT, "
            + IS_FAST + " INTEGER, "
            + CREATE_DATE + " INTEGER, "
            + UPDATE_DATE + " INTEGER );";
    
    /**
     * Insert new routine name, formed as New Routine <lastId +1>, so last id is incremented by 1 and 
     * appended to a new playlist name
     * 
     * @return id of new routine
     */
    public long insertPlaylist() {
        
        //get last playlists id, increment and add to the playlist name
        StringBuffer selectBuffer = new StringBuffer("select max(_id) from ");
        selectBuffer.append(DB_PLAYLISTS_TABLE + ";");
        Cursor cursor = db.rawQuery(selectBuffer.toString(),null);
        boolean hasValue = cursor.moveToFirst(); 
        int nextId = 0;
        if(hasValue) {
            nextId = (cursor.isNull(0))?0:cursor.getInt(0)+1;
        }
        cursor.close();
        

        Calendar calendar = Calendar.getInstance();
        // update with current date
        SimpleDateFormat createDateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        ContentValues vals = new ContentValues();
        vals.put(
                NAME,
                NEW_PLAYLIST_NAME + nextId);
        vals.put(CREATE_DATE, createDateFormat.format(calendar.getTime()));
        vals.put(UPDATE_DATE, createDateFormat.format(calendar.getTime()));
        return db.insert(DB_PLAYLISTS_TABLE, null, vals);
    }
    
    /**
     * match by id, populate Routine and return it, return empty Routine if no
     * match
     * 
     * @param playlistId
     * @return
     */
    public Playlist getPlaylistById(long playlistId) {
        StringBuffer selectBuffer = new StringBuffer("select * from ");
        selectBuffer.append(DB_PLAYLISTS_TABLE);
        selectBuffer.append(" where " + DBPlaylistAdapter.ID + "=?");
        Cursor cursor = db.rawQuery(selectBuffer.toString(),
                new String[] { String.valueOf(playlistId) });
        Playlist playlist = Playlist.populateFirstRecord(cursor);
        cursor.close();
        return playlist;
    }
    
    /**
     * will update playlist record, if that field was set when passed
     * 
     * @param playlist
     *            Playlist
     * @return n of rows updated
     */
    public int updatePlaylist(Playlist playlist) {
        ContentValues cv = new ContentValues();
        if (playlist.getName() != null) {
            cv.put(NAME, playlist.getName());
        }
        if (playlist.getDescription() != null) {
            cv.put(PLAYLIST_DESCRIPTION, playlist.getDescription());
        }
       
        // update with current date
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        cv.put(UPDATE_DATE, dateFormat.format(date));
        
        cv.put(IS_FAST, playlist.getFastInt());

        int nRows = db.update(DB_PLAYLISTS_TABLE, cv, ID + "=?",
                new String[] { String.valueOf(playlist.getId()) });

        return nRows;
    }
    
    /**
     * get all playlists, ordered by last update date, fast or slow track depending on isFast.
     * @param isFast
     * @return
     */
    public ArrayList<ListItem> getPlaylists(boolean isFast){
    	int isFastInt = (isFast)?1:0;
    	ArrayList<ListItem> plList = new ArrayList<ListItem>();
    	StringBuffer selectBuffer = new StringBuffer("select * from ");
        selectBuffer.append(DB_PLAYLISTS_TABLE);
        selectBuffer.append(" where " + DBPlaylistAdapter.IS_FAST + "=? order by update_date desc");
        Cursor cursor = db.rawQuery(selectBuffer.toString(),
                new String[] { String.valueOf(isFastInt) });
        cursor.moveToFirst();
        
        while (cursor.isAfterLast() == false) {
        	plList.add(new ListItem(cursor.getString(cursor.getColumnIndex(ID)), 
        			cursor.getString(cursor.getColumnIndex(NAME)),
        			cursor.getString(cursor.getColumnIndex(PLAYLIST_DESCRIPTION))));
       	    cursor.moveToNext();
        }
		
		cursor.close();
    	return plList;
    }
    
    public void deleteAllPlaylists(boolean isFast) {
    	int isFastInt = (isFast)?1:0;
	    this.db.delete(DB_PLAYLISTS_TABLE, IS_FAST + "=?", new String[] {String.valueOf(isFastInt)});
	    db.close();
	}
    
    public void deleteSelectedPlaylists(boolean isFast, List<Integer> selectedPlaylists){
    	int isFastInt = (isFast)?1:0;
    	StringBuffer idBuf = new StringBuffer(ID);
    	String[] values = new String[selectedPlaylists.size() +1];
    	values[0] = String.valueOf(isFastInt);
    	idBuf.append(" in(");
    	for(int i=0; i<selectedPlaylists.size(); i++){
    		idBuf.append("?");
    		//last element
    		if(i == selectedPlaylists.size()-1) {
    			idBuf.append(")");
    		} else {
    			idBuf.append(",");
    		}
    		values[i+1] = selectedPlaylists.get(i).toString();
    	}
    	this.db.delete(DB_PLAYLISTS_TABLE, IS_FAST + "=? and " + idBuf.toString(), values);
	    db.close();
    }

}
