package com.running.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.running.model.Routine;
import com.running.model.SpinnerValue;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

/*
 * DBRoutineAdapter
 * Provides an interface through which we can perform queries
 * against the SQLite database.
 */

public class DBRoutineAdapter extends DBAdapterBase{

	public static final String ROUTINE_WARMUP_TIME = "warmup_time";
	public static final String ROUTINE_TOTAL_DISTANCE = "total_distance";
	public static final String ROUTINE_SPRINT_DISTANCE = "sprint_distance";
	public static final String ROUTINE_REGULAR_PACE = "regular_pace";
	public static final String ROUTINE_SPRINT_PACE = "sprint_pace";
	public static final String ROUTINE_RECOVERY_TIME = "recovery_time";

	public static final String ROUTINE_IS_METRIC = "is_metric";
	public static final String NEW_ROUTINE_NAME = "New Routine";
	public static final String DB_ROUTINES_TABLE = "routines";
	
	

	// a SQL statement to create a new table
	// 0 false, 1 true, metric false by default
	private static final String DB_ROUTINE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ DB_ROUTINES_TABLE + " (" + ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " 
			+ NAME + " TEXT NOT NULL, " 
			+ CREATE_DATE + " INTEGER, "
			+ UPDATE_DATE + " INTEGER, " 
			+ ROUTINE_TOTAL_DISTANCE
			+ " REAL, " + ROUTINE_IS_METRIC + " INTEGER, "
			+ ROUTINE_SPRINT_DISTANCE + " REAL, " 
			+ ROUTINE_SPRINT_PACE + " REAL, " 
			+ ROUTINE_REGULAR_PACE + " REAL, "
			+ ROUTINE_RECOVERY_TIME + " REAL, "
			+ ROUTINE_WARMUP_TIME + " REAL)";

	public DBRoutineAdapter(Context c) {
	    super(c);
	}


	 /**
     * Open the DB, or throw a SQLException if we cannot open or create a new
     * DB.
     */
    public DBAdapterBase open() throws SQLException {
        
        // instantiate a DatabaseHelper class (see above)
        this.helper = new DatabaseHelper(context, DB_ROUTINES_TABLE, DB_ROUTINE_CREATE);

        // the SQLiteOpenHelper class (a parent of DatabaseHelper)
        // has a "getWritableDatabase" method that returns an
        // object of type SQLiteDatabase that represents an open
        // connection to the database we've opened (or created).
        db = this.helper.getWritableDatabase();

        return this;
    }
    

	/**
	 * Insert new routine name, formed as New Routine - <dd-MM-yyyy>
	 * 
	 * @return id of new routine
	 */
	public long insertRoutineRecord() {
		Calendar calendar = Calendar.getInstance();
		// update with current date
		SimpleDateFormat createDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		ContentValues vals = new ContentValues();
		vals.put(
				NAME,
				NEW_ROUTINE_NAME );
		//default values
		vals.put(ROUTINE_TOTAL_DISTANCE, new Float(0));
		vals.put(ROUTINE_IS_METRIC, new Boolean(false));
		vals.put(ROUTINE_SPRINT_DISTANCE, new Float(0));
		vals.put(ROUTINE_SPRINT_PACE, new Float(0));
		vals.put(ROUTINE_REGULAR_PACE, new Float(0));
		vals.put(ROUTINE_RECOVERY_TIME, new Float(0));
		vals.put(ROUTINE_WARMUP_TIME, new Float(0));
		vals.put(CREATE_DATE, createDateFormat.format(calendar.getTime()));
		vals.put(UPDATE_DATE, createDateFormat.format(calendar.getTime()));
		return db.insert(DB_ROUTINES_TABLE, null, vals);
	}

	/**
	 * will update routine record, if that field was set on passed Routine
	 * 
	 * @param routine
	 *            Routine
	 * @return n of rows updated
	 */
	public int updateRoutine(Routine routine) {
		ContentValues cv = new ContentValues();
		if (routine.getName() != null) {
			cv.put(NAME, routine.getName());
		}
		if (routine.getRecoveryTime() != null) {
			cv.put(ROUTINE_RECOVERY_TIME, routine.getRecoveryTime());
		}
		if (routine.getWarmupTime() != null) {
			cv.put(ROUTINE_WARMUP_TIME, routine.getWarmupTime());
		}
		if (routine.isMetric() != null) {
			cv.put(ROUTINE_IS_METRIC, routine.isMetric());
		}
		if (routine.getSprintDistance() != null) {
			cv.put(ROUTINE_SPRINT_DISTANCE, routine.getSprintDistance());
		}
		if (routine.getSprintPace() != null) {
			cv.put(ROUTINE_SPRINT_PACE, routine.getSprintPace());
		}
		if (routine.getRegularPace() != null) {
			cv.put(ROUTINE_REGULAR_PACE, routine.getRegularPace());
		}
		if (routine.getTotalDistance() != null) {
			cv.put(ROUTINE_TOTAL_DISTANCE, routine.getTotalDistance());
		}
		// update with current date
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		cv.put(UPDATE_DATE, dateFormat.format(date));

		int nRows = db.update(DB_ROUTINES_TABLE, cv, ID + "=?",
				new String[] { String.valueOf(routine.getId()) });

		return nRows;
	}
	
	public void deleteRoutine(long routineId) {
	    this.db.delete(DB_ROUTINES_TABLE, ID + "=?", new String[] {String.valueOf(routineId)});
	    db.close();
	}

	/**
	 * match by id, populate Routine and return it, return empty Routine if no
	 * match
	 * 
	 * @param routineId
	 * @return
	 */
	public Routine getRoutineById(long routineId) {
		StringBuffer selectBuffer = new StringBuffer("select * from ");
		selectBuffer.append(DB_ROUTINES_TABLE);
		selectBuffer.append(" where " + DBAdapterBase.ID + "=?");
		// Cursor cursor = db.query(DB_ROUTINES_TABLE, new
		// String[]{DBRoutineAdapter.ID}, null, null, null, null, null);
		Cursor cursor = db.rawQuery(selectBuffer.toString(),
				new String[] { String.valueOf(routineId) });
		Routine routine = Routine.populateFirstRecord(cursor);
		cursor.close();
		return routine;
	}

	public List<SpinnerValue> getRoutinesSpinnerList() {
		List<SpinnerValue> routinesList =  new ArrayList<SpinnerValue>();
		
		//order by date?
		Cursor cur = db.query(DB_ROUTINES_TABLE, null, null, null, null, null, UPDATE_DATE + " DESC");
		cur.moveToFirst();
        while (cur.isAfterLast() == false) {
        	//Routine routine = new Routine();
        	//Routine.populateCurrent(cur, routine);
        	routinesList.add(new SpinnerValue(cur.getInt(cur.getColumnIndex(ID)), cur.getString(cur.getColumnIndex(NAME))));
       	    cur.moveToNext();
        }
		
		cur.close();
		return routinesList;
	}

	

}
