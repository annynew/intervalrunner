package com.running.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.running.model.FileItem;
import com.running.model.ListItem;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;

/**
 * DB class representing file
 * 
 * @author amouradi
 * 
 */
public class DBFilesAdapter extends DBAdapterBase {

	// define the layout of our table in fields
	// "_id" is used by Android for Content Providers and should
	// generally be an auto-incrementing key in every table.
	public static final String COMPOSER = "composer";
	public static final String TITLE = "title";
	public static final String DURATION = "duration";
	public static final String MEDIA_ID = "media_id";

	public static final String DB_FILES_TABLE = "files";

	public DBFilesAdapter(Context c) {
		super(c);
	}

	@Override
	public DBAdapterBase open() throws SQLException {

		this.helper = new DatabaseHelper(context, DB_FILES_TABLE,
				DB_FILES_CREATE);
		db = this.helper.getWritableDatabase();
		return this;
	}

	// a SQL statement to create a new file table
	private static final String DB_FILES_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ DB_FILES_TABLE + " (" + ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE
			+ " TEXT NOT NULL, " + COMPOSER + " TEXT, " + DURATION
			+ " INTEGER, " + MEDIA_ID + " TEXT NOT NULL );";

	/**
	 * Insert new file name
	 * 
	 * @return id of new file record
	 */
	public long insertFile(String title, String composer, Integer duration,
			String mediaId) {

		ContentValues vals = new ContentValues();
		vals.put(TITLE, title);
		vals.put(COMPOSER, composer);
		vals.put(DURATION, duration);
		vals.put(COMPOSER, composer);
		return db.insert(DB_FILES_TABLE, null, vals);
	}

	/**
	 * get all files, in the order they have been added
	 * 
	 * @param isFast
	 * @return
	 */
	public List<FileItem> getFiles() {
		ArrayList<FileItem> filesList = new ArrayList<FileItem>();
		StringBuffer selectBuffer = new StringBuffer("select * from ");
		selectBuffer.append(DB_FILES_TABLE);
		Cursor cursor = db.rawQuery(selectBuffer.toString(), null);
		cursor.moveToFirst();

		while (cursor.isAfterLast() == false) {
			filesList.add(new FileItem(
					String.valueOf(cursor.getLong(cursor.getColumnIndex(ID))), cursor
							.getString(cursor.getColumnIndex(TITLE)), cursor
							.getString(cursor.getColumnIndex(COMPOSER)), cursor
							.getInt(cursor.getColumnIndex(DURATION)), cursor
							.getString(cursor.getColumnIndex(MEDIA_ID))));
			cursor.moveToNext();
		}

		cursor.close();
		return filesList;
	}

	public List<FileItem> getLocalFiles(ContentResolver contentResolver) {
		List<FileItem> filesList = new ArrayList<FileItem>();
		Uri uri = android.provider.MediaStore.Audio.Media
				.getContentUriForPath("/sdcard");
		Cursor cursor = contentResolver.query(uri, null, null, null, null);
		if (cursor == null) {
			// query failed, handle error.
		} else if (!cursor.moveToFirst()) {
			// no media on the device
		} else {
			int titleColumn = cursor
					.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
			int idColumn = cursor
					.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
			int durationColumn = cursor
					.getColumnIndex(android.provider.MediaStore.Audio.Media.DURATION);
			 int composerColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.COMPOSER);
			 //int displayNameColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.DISPLAY_NAME);
			 //int albumColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ALBUM);
			 
			while(cursor.isAfterLast() == false) {
				long mediaId = cursor.getLong(idColumn);
				
				String title = cursor.getString(titleColumn);
				//String displayName = cursor.getString(displayNameColumn);
				//String album = cursor.getString(albumColumn);
				
				int durationInt = cursor.getInt(durationColumn);
				String composerStr = cursor.getString(composerColumn);
				filesList.add(new FileItem(String.valueOf(mediaId), title, composerStr, durationInt, String.valueOf(mediaId)));
				cursor.moveToNext();
			}
		}
		return filesList;
	}

	public void deleteAllFiles() {
		this.db.delete(DB_FILES_TABLE, null, null);
		db.close();
	}

	public void deleteSelectedFiles(List<Integer> selectedFiles) {
		StringBuffer idBuf = new StringBuffer(ID);
		String[] values = new String[selectedFiles.size() + 1];
		idBuf.append(" in(");
		for (int i = 0; i < selectedFiles.size(); i++) {
			idBuf.append("?");
			// last element
			if (i == selectedFiles.size() - 1) {
				idBuf.append(")");
			} else {
				idBuf.append(",");
			}
			values[i + 1] = selectedFiles.get(i).toString();
		}
		this.db.delete(DB_FILES_TABLE, idBuf.toString(), values);
		db.close();
	}

}
