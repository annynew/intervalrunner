package com.running.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public abstract class DBAdapterBase {

 // define some SQLite database fields
    // Take a look at your DB on the emulator with:
    // adb shell
    // sqlite3 /data/data/com.running/databases/db_runner
	//.tables
    protected static final String DB_NAME = "db_runner";
    protected static final int DB_VER = 1;
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String CREATE_DATE = "create_date";
    public static final String UPDATE_DATE = "update_date";

    protected final Context context;
    protected DatabaseHelper helper;
    protected SQLiteDatabase db;
    
    public DBAdapterBase(Context c){
        this.context = c;
    }
    
    /**
     * Open the DB, or throw a SQLException if we cannot open or create a new
     * DB.
     */
    public abstract DBAdapterBase open() throws SQLException; 
    

    /**
     * Close the DB
     */
    public void close() {
        this.helper.close();
    }
    
    public boolean isOpen(){
    	return this.db.isOpen();
    }
    
    public static class DatabaseHelper extends SQLiteOpenHelper {
        private String tableName;
        private String createSql;

        DatabaseHelper(Context context, String tableName, String createSql) {
            super(context, DB_NAME, null, DB_VER);
            this.tableName = tableName;
            this.createSql = createSql;
        }

        // called by the parent class when a DB doesn't exist
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(this.createSql);
        }
        
        

        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            //create table if does not exist
            db.execSQL(this.createSql);
        }

        // called by the parent when a DB needs to be upgraded
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
            // remove the old version and create a new one.
            // If we were really upgrading we'd try to move data over
            db.execSQL("DROP TABLE IF EXISTS " + this.tableName);
            onCreate(db);
        }
        
        
    }
}
