package com.running.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.running.R;
import com.running.model.Item;
import com.running.model.ListItem;


public class ListViewAdapter extends ArrayAdapter<Object> {
	
	protected List<? extends Item> items;
	protected int itemLayout;
	protected  LayoutInflater vi ;

    public ListViewAdapter(Context context, int textViewResourceId, List<? extends Item> items) {
            super(context, textViewResourceId, items.toArray());
            this.items =  items;
            this.itemLayout = textViewResourceId;
            vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            //View view = convertView;
            View view = vi.inflate(itemLayout, null);
            ListItem currentItem = (ListItem)items.get(position);
            
            if (currentItem != null) {
                    TextView itemTitle = (TextView) view.findViewById(R.id.toptext);
                    TextView bottomText = (TextView) view.findViewById(R.id.bottomtext);
                    if (itemTitle != null) {
                          itemTitle.setText(currentItem.getTitle());     
                          if(currentItem.getColor() != null){
                        	  itemTitle.setTextColor(Color.parseColor(currentItem.getColor()));
                          }
                    }
                    if(bottomText != null && currentItem.getBottomText() != null && currentItem.getBottomText().length() > 0){
                    	 bottomText.setText(currentItem.getBottomText());
                    	 if(currentItem.getColor() != null){
                    		 bottomText.setTextColor(Color.parseColor(currentItem.getColor()));
                         }
                    } else {
                    	bottomText.setText("");
                    }
            }
            return view;
    }

}
