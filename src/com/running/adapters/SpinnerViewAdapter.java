package com.running.adapters;

import java.util.List;

import com.running.R;
import com.running.model.SpinnerValue;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

/**
 * Display spinner element
 * 
 * @author amouradi
 * 
 */
public class SpinnerViewAdapter extends ArrayAdapter<Object> {

	private List<SpinnerValue> items;

	public SpinnerViewAdapter(Context context, int textViewResourceId,
			List<SpinnerValue> items) {
		super(context, textViewResourceId, items.toArray());
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			view = vi.inflate(R.layout.custom_spinner_item, null);
		}
		SpinnerValue currentItem = items.get(position);
		if (currentItem != null) {
			TextView itemTitle = (TextView) view.findViewById(R.id.spinnerText);
			if (itemTitle != null) {
				itemTitle.setText(currentItem.getName());
			}
		}
		return view;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		CheckedTextView view = (CheckedTextView)super.getDropDownView(position, convertView, parent);
		
		SpinnerValue currentItem = items.get(position);
		view.setText(currentItem.getName());
		
		return view;

	}

	@Override
	public long getItemId(int position) {
		SpinnerValue currentItem = items.get(position);
		
		return currentItem.getId();
	}
	
	
}
