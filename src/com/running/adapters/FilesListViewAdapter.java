package com.running.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.running.R;
import com.running.model.FileItem;

public class FilesListViewAdapter extends ArrayAdapter<FileItem> {

	private Context context;
	private int id;
	private List<FileItem> items;

	public FilesListViewAdapter(Context context, int textViewResourceId,
			List<FileItem> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		id = textViewResourceId;
		items = objects;
	}

	public FileItem getItem(int i) {
		return items.get(i);
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(id, null);
            }
            final FileItem fileItem = items.get(position);
            if (fileItem != null) {
                    TextView topText = (TextView) v.findViewById(R.id.toptext);
                    TextView bottomText = (TextView) v.findViewById(R.id.bottomtext);
                    
                    if(topText!=null) {
                    	topText.setText(fileItem.getTitle());
                    }
                    if(bottomText!=null) {
                    	bottomText.setText(fileItem.getDuration());
                    }
                    
            }
            return v;
    }

}
