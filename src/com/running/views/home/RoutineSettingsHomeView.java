/**
 * 
 */
package com.running.views.home;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.running.R;
import com.running.adapters.SpinnerViewAdapter;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.SpinnerValue;

/**
 * Home screen with the Routines already created, UI to edit create new
 * routines. Settings Menu
 * 
 * @author amouradian
 * 
 */
public class RoutineSettingsHomeView extends RoutinesBase implements OnClickListener {

    private DBRoutineAdapter db;
    private ImageButton newRoutineBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routines_settings_home);
        // create table if it does not exist
        db = new DBRoutineAdapter(this);

        // open a connection to the DB, and create
        // a table if one does not yet exist.
        db.open();

        this.prepareScreen();

       
    }

    private void prepareScreen() {

    	 newRoutineBtn = (ImageButton) this.findViewById(R.id.btn_new_routine);
         newRoutineBtn.setOnClickListener(this);
         
        Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
        List<SpinnerValue> routines = db.getRoutinesSpinnerList();
        TextView routinesPrompt = (TextView) findViewById(R.id.txt_routine_prompt);
        //edit button
        ImageButton editRoutineBtn = (ImageButton) this.findViewById(R.id.btn_edit_routine);
        LinearLayout editLayout = (LinearLayout)this.findViewById(R.id.edit_layout);
        
        //delete button
        ImageButton deleteRoutineBtn = (ImageButton) this.findViewById(R.id.btn_delete_routine);
        LinearLayout deleteLayout = (LinearLayout)this.findViewById(R.id.delete_layout);
        
        //preview run button
        ImageButton previewRunBtn = (ImageButton) this.findViewById(R.id.btn_preview_run);
        LinearLayout previewLayout = (LinearLayout)this.findViewById(R.id.preview_and_run_layout);

        ImageButton runBtn = (ImageButton) this.findViewById(R.id.btn_run);
        LinearLayout runLayout = (LinearLayout)this.findViewById(R.id.run_layout);
        
        // show edit routine button and spinner if there are routines
        if (routines.size() > 0) {
            spinner.setVisibility(View.VISIBLE);
            routinesPrompt.setText(getString(R.string.routine_prompt));
            routinesPrompt.setVisibility(View.VISIBLE);
           
            editLayout.setVisibility(View.VISIBLE);
            editRoutineBtn.setOnClickListener(this);
            
            deleteLayout.setVisibility(View.VISIBLE);
            deleteRoutineBtn.setOnClickListener(this);
            
            previewLayout.setVisibility(View.VISIBLE);
            previewRunBtn.setOnClickListener(this);
            
            runLayout.setVisibility(View.VISIBLE);
            runBtn.setOnClickListener(this);
            
            SpinnerViewAdapter adapter = new SpinnerViewAdapter(this, android.R.layout.simple_spinner_item, routines);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new SpinnerItemSelectedListener());
        } else {
            spinner.setVisibility(View.GONE);
            editLayout.setVisibility(View.GONE);
            deleteLayout.setVisibility(View.GONE);
            previewLayout.setVisibility(View.GONE);
            runLayout.setVisibility(View.GONE);
            routinesPrompt.setText(getString(R.string.txt_no_routines));
        }
        
    }

    public void onRestart() {
        super.onRestart();
        if(db == null) {
        	new DBRoutineAdapter(this);
        	db.open();
        }
        prepareScreen();
    }

    @Override
    public void onClick(View v) {

        int buttonId = ((ImageButton)v).getId();
        long routineId;
        
        if(buttonId == R.id.btn_new_routine) {
            // create new routine and save the record
            routineId = db.insertRoutineRecord();
            // pass new routineId to the next settings intent
            Bundle bundle = new Bundle();
            bundle.putLong(DBAdapterBase.ID, routineId);

            Intent intent = new Intent(v.getContext(), RoutineSettingsListView.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (buttonId == R.id.btn_edit_routine){
            Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
            SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
            routineId = selectedRoutine.getId();
            // pass new routineId to the next settings intent
            Bundle bundle = new Bundle();
            bundle.putLong(DBAdapterBase.ID, routineId);

            Intent intent = new Intent(v.getContext(), RoutineSettingsListView.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (buttonId == R.id.btn_delete_routine){
            Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
            SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
            routineId = selectedRoutine.getId();
            db.deleteRoutine(routineId);
            Toast.makeText(this,"Delete succeeded.", Toast.LENGTH_SHORT).show(); 
            //reload routines
            db.open();
            prepareScreen();
        }
        else if(buttonId == R.id.btn_preview_run) {
        	 Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
             SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
             routineId = selectedRoutine.getId();
             
             // pass new routineId to the next settings intent
             Bundle bundle = new Bundle();
             bundle.putLong(DBAdapterBase.ID, routineId);

             Intent intent = new Intent(v.getContext(), PreviewRunView.class);
             intent.putExtras(bundle);
             startActivity(intent);
        }
        else if(buttonId == R.id.btn_run){
        	Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
            SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
        	 Bundle bundle = new Bundle();
             bundle.putLong(DBRoutineAdapter.ID, selectedRoutine.getId());

             Intent intent = new Intent(v.getContext(), RunCountDownView.class);
             intent.putExtras(bundle);
             startActivity(intent);
        }
    }


    /**
     * Explicitly close our database connection when our application is done
     * with it and we're about to quit.
     */
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }

    class SpinnerItemSelectedListener implements OnItemSelectedListener {

        //TODO not needed?
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }
}
