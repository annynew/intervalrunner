package com.running.views.home;

import android.os.Bundle;
import android.widget.EditText;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * sprint distance
 * @author amouradian
 *
 */
public class SprintDistanceEditView extends TextValidator {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	 EditText sprintDistanceEdit;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sprint_distance_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        sprintDistanceEdit = (EditText) this.findViewById(R.id.editSprintDistanceId);
        sprintDistanceEdit.setText(routine.getSprintDistance().toString());
    }

	@Override
	public void onPause() {
		super.onPause();
		
		Routine routine = new Routine();
		routine.setId(routineId);
		routine.setSprintDistance(replaceNullWithZero(sprintDistanceEdit.getText().toString()));
		dbAdapter.updateRoutine(routine);
	}
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}


}
