package com.running.views.home;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

/**
 * base class for screens that need validators. Contains common validation and db persistence methods.
 * 
 * @author amourad
 *
 */
public abstract class TextValidator extends Activity implements TextWatcher{
	
	

	public void validateNonNull(TextView errText, String value) {
		try {
			if(value.trim().length() == 0 
					|| new Float(value).floatValue() == 0f){
				errText.setVisibility(View.VISIBLE);
			} else {
				errText.setVisibility(View.INVISIBLE);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			errText.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}
	
	public Float replaceNullWithZero(String value) {
		
		if(value.trim().length() <= 0 ) {
			value = "0.0";
		}
		return new Float(value);
	}
}
