/**
 * 
 */
package com.running.views.home;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.running.R;
import com.running.adapters.SpinnerViewAdapter;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.SpinnerValue;

/**
 * Screen displayed when Run button is clicked on a home dashboard
 * 
 * @author amouradian
 * 
 */  
public class RoutinesSelectHomeView extends RoutinesBase implements OnClickListener {

    private DBRoutineAdapter db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.run_select_routines_layout);
        // create table if it does not exist
        db = new DBRoutineAdapter(this);

        // open a connection to the DB, and create
        // a table if one does not yet exist.
        db.open();

        this.createRoutinesSpinner();

      
    }

    private void createRoutinesSpinner() {

        Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
        List<SpinnerValue> routines = db.getRoutinesSpinnerList();
        TextView noRoutines = (TextView) findViewById(R.id.txt_no_routines);
        
        TextView routinesPrompt = (TextView) findViewById(R.id.txt_routine_prompt);
       //preview_and_run_layout
        LinearLayout previewAndRunLayout = (LinearLayout)findViewById(R.id.preview_and_run_layout);
        
        //preview run button
        ImageButton previewRunBtn = (ImageButton) this.findViewById(R.id.btn_preview_run);
        LinearLayout runLayout = (LinearLayout)findViewById(R.id.run_layout);
        previewRunBtn.setOnClickListener(this);

        ImageButton runBtn = (ImageButton) this.findViewById(R.id.btn_run);
        runBtn.setOnClickListener(this);
        
        // show edit routine button and spinner if there are routines
        if (routines.size() > 0) {
        	noRoutines.setVisibility(View.INVISIBLE);
            spinner.setVisibility(View.VISIBLE);
            routinesPrompt.setVisibility(View.VISIBLE);
            previewAndRunLayout.setVisibility(View.VISIBLE);
            runLayout.setVisibility(View.VISIBLE);
            
            SpinnerViewAdapter adapter = new SpinnerViewAdapter(this, android.R.layout.simple_spinner_item, routines);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new SpinnerItemSelectedListener());
        } else {
        	
        	noRoutines.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.INVISIBLE);
            routinesPrompt.setVisibility(View.INVISIBLE);
            previewAndRunLayout.setVisibility(View.INVISIBLE);
            runLayout.setVisibility(View.INVISIBLE);
        }
        
    }

    public void onRestart() {
        super.onRestart();
        if(db == null) {
        	new DBRoutineAdapter(this);
        	db.open();
        }
        createRoutinesSpinner();
    }

    @Override
    public void onClick(View v) {

        int buttonId = ((ImageButton)v).getId();
        long routineId;
        
        if(buttonId == R.id.btn_preview_run) {
        	 Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
             SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
             routineId = selectedRoutine.getId();
             
             // pass new routineId to the next settings intent
             Bundle bundle = new Bundle();
             bundle.putLong(DBAdapterBase.ID, routineId);

             Intent intent = new Intent(v.getContext(), PreviewRunView.class);
             intent.putExtras(bundle);
             startActivity(intent);
        }
        else if(buttonId == R.id.btn_run){
        	Spinner spinner = (Spinner) findViewById(R.id.routinesSpinner);
            SpinnerValue selectedRoutine = (SpinnerValue)spinner.getSelectedItem();
        	 Bundle bundle = new Bundle();
             bundle.putLong(DBRoutineAdapter.ID, selectedRoutine.getId());

             Intent intent = new Intent(v.getContext(), RunCountDownView.class);
             intent.putExtras(bundle);
             startActivity(intent);
        }
    }


    /**
     * Explicitly close our database connection when our application is done
     * with it and we're about to quit.
     */
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }

    class SpinnerItemSelectedListener implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }
}
