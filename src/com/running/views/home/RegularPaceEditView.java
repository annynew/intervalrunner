package com.running.views.home;

import android.os.Bundle;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * reg pace
 * @author amouradian
 *
 */
public class RegularPaceEditView extends TextValidator {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	 private EditText regularPaceEdit;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regular_pace_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        regularPaceEdit = (EditText) this.findViewById(R.id.editRegularPaceId);
        regularPaceEdit.setText(routine.getRegularPace().toString());
        regularPaceEdit.addTextChangedListener(this);
    }

	@Override
	public void onPause() {
		super.onPause();
		Routine routine = new Routine();
		routine.setId(routineId);
		
		routine.setRegularPace(replaceNullWithZero(regularPaceEdit.getText().toString()));
		dbAdapter.updateRoutine(routine);
	}
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}

	@Override
	public void afterTextChanged(Editable editField) {
		
		TextView errText =  (TextView)this.findViewById(R.id.validate_not_null);
		validateNonNull(errText, editField.toString());
	}

}
