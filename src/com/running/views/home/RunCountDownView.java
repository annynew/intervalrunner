/**
 * 
 */
package com.running.views.home;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Lap;
import com.running.model.Routine;
import com.running.model.Run;
import com.running.utils.TimeConverter;

/**
 * Preview your run screen, just display calculated run
 * 
 * @author amouradian
 * 
 */
public class RunCountDownView extends RoutinesBase implements OnClickListener {

	private DBRoutineAdapter db;
	private Float mileInMeter = 0.000621371f;
	private boolean timerPaused = false;
	private Run run;
	private Handler handler = new Handler();
	private long m_startMillis = 0;
	private UpdateTimeTask updateTimeTask;
	private TextView totalElapsedSecondsComp;
	private TextView totalElapsedMinutesComp;
	private TextView totalElapsedHoursComp;
	private Button pauseBtn;
	private Button stopBtn;
	private long m_lapStartMillis;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.run_countdown);
		// create table if it does not exist
		db = new DBRoutineAdapter(this);

		// open a connection to the DB, and create
		// a table if one does not yet exist.
		db.open();

		this.prepareScreen();
		stopBtn = (Button) this.findViewById(R.id.run_countdown_btn_stop);
		stopBtn.setOnClickListener(this);

		pauseBtn = (Button) this.findViewById(R.id.run_countdown_btn_pause);
		pauseBtn.setOnClickListener(this);
	}

	private void prepareScreen() {
		Bundle bundle = getIntent().getExtras();
		Long routineId = bundle.getLong(DBAdapterBase.ID);
		float milesRunningTotal = 0f;

		// load routine from database
		Routine routine = db.getRoutineById(routineId);
		
		//if invalid values, go to PreviewRun
		if(routine.getTotalDistance() == null || routine.getTotalDistance() == 0f || 
				routine.getRegularPace() == null || routine.getRegularPace() == 0f ||
				routine.getRecoveryTime() == null || routine.getRecoveryTime() == 0f) {
			
			 // pass new routineId to the next settings intent
            bundle.putLong(DBAdapterBase.ID, routineId);

            Intent intent = new Intent(this, PreviewRunView.class);
            intent.putExtras(bundle);
            startActivity(intent);
		}

		// start calculating
		run = new Run();
		float totalTime = 0f;

		// add warmup lap

		Lap warmupLap = new Lap();
		warmupLap.setTitle("warmup");
		warmupLap.setLapMinutes(routine.getWarmupTime());
		warmupLap.setLapMiles(routine.getWarmupTime() * 1 / routine.getRegularPace());
		milesRunningTotal += warmupLap.getLapMiles();
		totalTime += warmupLap.getLapMinutes();
		run.getListOfLaps().add(warmupLap);

		boolean isSprint = true; // start with sprint after warmup

		float sprintMiles = routine.getSprintDistance() * mileInMeter;
		float sprintMinutes = routine.getSprintPace() * sprintMiles / 1;
		float regularMiles = routine.getRecoveryTime() * 1 / routine.getRegularPace();


		// calculate and prepare Run with collection of laps
		while (milesRunningTotal < routine.getTotalDistance()) {
			Lap lap = null;
			if (isSprint) {
				// sprint minutes and distance
				milesRunningTotal = buildLap(milesRunningTotal, "sprint",
						sprintMinutes, sprintMiles, routine.getTotalDistance(),
						routine.getSprintPace(), run);
				lap = run.getListOfLaps().get(run.getListOfLaps().size() - 1);
			} else {
				milesRunningTotal = buildLap(milesRunningTotal, "regular",
						routine.getRecoveryTime(), regularMiles,
						routine.getTotalDistance(), routine.getRegularPace(), run);
				lap = run.getListOfLaps().get(run.getListOfLaps().size() - 1);
			}
			totalTime += lap.getLapMinutes();
			isSprint = !isSprint;
		}
		run.setTotalTime(totalTime);
		
		if (m_startMillis == 0L) {
			m_startMillis = System.currentTimeMillis();
			updateTimeTask = new UpdateTimeTask();
			handler.removeCallbacks(updateTimeTask);
			handler.postDelayed(updateTimeTask, 1000);
		}

	}

	private float buildLap(float milesRunningTotal, String lapTitle, Float lapMinutes,
			Float lapDistance, Float totalDistance, Float lapPace, Run run) {
		// sprint minutes and distance
		Lap lap = new Lap();
		lap.setTitle(lapTitle);

		if ((milesRunningTotal + lapDistance) >= totalDistance) {
			float milesLeft = totalDistance - milesRunningTotal;
			lap.setLapMiles(milesLeft);

			lap.setLapMinutes(milesLeft * lapPace);
			milesRunningTotal += milesLeft;
		} else {
			lap.setLapMinutes(lapMinutes);
			lap.setLapMiles(lapDistance);
			milesRunningTotal += lapDistance;
		}
		run.getListOfLaps().add(lap);
		run.setTotalTime(milesRunningTotal);
		return milesRunningTotal;
	}

	/**
	 * Explicitly close our database connection when our application is done
	 * with it and we're about to quit.
	 */
	public void onDestroy() {
		super.onDestroy();
		handler.removeCallbacks(updateTimeTask);
		db.close();
	}

	@Override
	public void onPause() {
		super.onPause();
		handler.removeCallbacks(updateTimeTask);
	}

	@Override
	public void onClick(View v) {
		int buttonId = ((Button) v).getId();


		if (buttonId == R.id.run_countdown_btn_stop) {

			handler.removeCallbacks(updateTimeTask);
			// disable Pause button
			pauseBtn.setEnabled(false);
			stopBtn.setEnabled(false);

		} else if (buttonId == R.id.run_countdown_btn_pause) {

			// pause
			if (!this.timerPaused) {
				pauseBtn.setText(getString(R.string.btn_resume));
				this.updateTimeTask.setDoPause(true);
				this.timerPaused = true;
				this.updateTimeTask.setPauseStartTime(System.currentTimeMillis());
			}
			// resume
			else {
				this.timerPaused = false;
				this.updateTimeTask.setDoPause(false);
				long currentElapsedPauseMillis = System.currentTimeMillis() - updateTimeTask.getPauseStartTime();
				long totalElapsedPauseTime = this.updateTimeTask.getPauseMillis() + currentElapsedPauseMillis; 
				this.updateTimeTask.setPauseMillis(totalElapsedPauseTime);
				
				long lapTotalElapsedPauseTime = this.updateTimeTask.getLapPauseMillis() + currentElapsedPauseMillis;
				this.updateTimeTask.setLapPauseMillis(lapTotalElapsedPauseTime);
				
				run.setTotalTime(run.getTotalTime()+ (totalElapsedPauseTime/1000)/60);
				pauseBtn.setText(getString(R.string.btn_pause));
			}
		}
	}

	// /new runnable implementation of the thread
	class UpdateTimeTask implements Runnable {

		private TextView totalElapsedMilSecondsComp;
		private TextView totalTime;
		int currentLapIndex = 0;
		private TextView currentLapText;
		private TextView lapSeconds;
		private TextView lapMinutes;
		private TextView lapHours;
		private long pauseMillis = 0;
		private long pauseStartTime = 0;
		private long lapPauseMillis = 0;
		
		

		private boolean doPause = false;

		UpdateTimeTask() {
			totalElapsedMilSecondsComp = (TextView) findViewById(R.id.run_countdown_tot_milseconds_elapsed);
			totalElapsedSecondsComp = (TextView) findViewById(R.id.run_countdown_tot_seconds_elapsed);
			totalElapsedMinutesComp = (TextView) findViewById(R.id.run_countdown_total_minutes_elapsed);
			totalElapsedHoursComp = (TextView) findViewById(R.id.run_countdown_total_hours_elapsed);

			lapHours = (TextView) findViewById(R.id.run_countdown_lap_elapsed_hours);
			lapMinutes = (TextView) findViewById(R.id.run_countdown_lap_minutes_elapsed);
			lapSeconds = (TextView) findViewById(R.id.run_countdown_lap_seconds_elapsed);

			// total time
			totalTime = (TextView) findViewById(R.id.run_countdown_total_time);
			totalTime.setText(TimeConverter.convertMinsToTimeStr(run.getTotalTime()));
			currentLapText = (TextView) findViewById(R.id.run_countdown_current_lap);
		}

		public void run() {
			// just don't do any updates while paused
			if (doPause) {
				handler.postDelayed(this, 1);
				return;
			}
			long startMillis = m_startMillis + pauseMillis;

			long currentTimeMillis = System.currentTimeMillis();
			long elapsed = currentTimeMillis - startMillis;

			long seconds = (elapsed / 1000);
			seconds = seconds % 60;
			long minutes = (elapsed / 1000) / 60;
			long hours = minutes / 60;

			totalElapsedMilSecondsComp.setText(new Long(elapsed).toString());
			totalElapsedSecondsComp.setText(TimeConverter.timeFormatter
					.format(new Long(seconds)));
			totalElapsedMinutesComp.setText(TimeConverter.timeFormatter
					.format(new Long(minutes)));
			totalElapsedHoursComp.setText(TimeConverter.timeFormatter.format(new Long(
					hours)));

			// laps
			Lap currentLap = run.getListOfLaps().get(currentLapIndex);
			if (currentLapIndex == 0) {
				m_lapStartMillis = startMillis;
				lapPauseMillis = 0;
			} 

			long lapMillisElapsed = currentTimeMillis - m_lapStartMillis - lapPauseMillis;
			long totalLapMillis = (long) (currentLap.getLapMinutes() * 60 * 1000);


			if (lapMillisElapsed / 1000 > totalLapMillis / 1000) {
				lapPauseMillis = 0;
				m_lapStartMillis = currentTimeMillis;
				currentLapIndex++;
				lapMillisElapsed = currentTimeMillis - m_lapStartMillis;
				System.out.println("---1 changing lapMillisElapsed=" + lapMillisElapsed);
				currentLap = run.getListOfLaps().get(currentLapIndex);

				// TODO check app settings before calling this
				/*Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				// 2 secs
				v.vibrate(2000);*/
				beep(R.raw.chime1);
			}
			
			long lapSecs = (lapMillisElapsed / 1000) % 60;
			long lapMinutes = (lapMillisElapsed / 1000) / 60;
			long lapHours = lapMinutes / 60;

			currentLapText.setText(currentLap.getTitle());
			this.lapSeconds.setText(TimeConverter.timeFormatter
					.format(new Long(lapSecs)));
			this.lapMinutes.setText(TimeConverter.timeFormatter.format(new Long(
					lapMinutes)));
			this.lapHours.setText(TimeConverter.timeFormatter
					.format(new Long(lapHours)));

			// repeat every sec
			if (elapsed <= (run.getTotalTime() * 60) * 1000) {
				handler.postDelayed(this, 1);
			} else {
				beep(R.raw.complete);
				pauseBtn.setEnabled(false);
				stopBtn.setEnabled(false);
			}
		}

		public boolean isDoPause() {
			return doPause;
		}

		public void setDoPause(boolean doPause) {
			this.doPause = doPause;
		}

		public long getPauseMillis() {
			return pauseMillis;
		}

		public void setPauseMillis(long pauseMillis) {
			this.pauseMillis = pauseMillis;
		}

		public long getPauseStartTime() {
			return pauseStartTime;
		}

		public void setPauseStartTime(long pauseStartTime) {
			this.pauseStartTime = pauseStartTime;
		}

		public long getLapPauseMillis() {
			return lapPauseMillis;
		}

		public void setLapPauseMillis(long lapPauseMillis) {
			this.lapPauseMillis = lapPauseMillis;
		}

	}

	private void beep(int fileName) {
		MediaPlayer mMediaPlayer = MediaPlayer.create(this, fileName);
		mMediaPlayer.start();
	}

}
