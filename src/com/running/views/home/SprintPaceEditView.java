package com.running.views.home;

import android.os.Bundle;
import android.widget.EditText;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * sprint pace
 * @author amouradian
 *
 */
public class SprintPaceEditView extends TextValidator {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	 private EditText sprintPaceEdit;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sprint_pace_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        sprintPaceEdit = (EditText) this.findViewById(R.id.editSprintPaceId);
        sprintPaceEdit.setText(routine.getSprintPace().toString());
    }

	@Override
	public void onPause() {
		super.onPause();
		
		Routine routine = new Routine();
		routine.setId(routineId);
		routine.setSprintPace(replaceNullWithZero(sprintPaceEdit.getText().toString()));
		dbAdapter.updateRoutine(routine);
	}
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}


}
