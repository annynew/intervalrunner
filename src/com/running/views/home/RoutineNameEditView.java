package com.running.views.home;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * enter routine name, saved in db
 * @author amouradian
 *
 */
public class RoutineNameEditView extends Activity {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routine_name_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        EditText routineNameEdit = (EditText) this.findViewById(R.id.editRoutineNameId);
        routineNameEdit.setText(routine.getName());
    }

	@Override
	public void onPause() {
		super.onPause();
		EditText routineNameEdit = (EditText) this.findViewById(R.id.editRoutineNameId);
		Routine routine = new Routine();
		routine.setId(routineId);
		routine.setName(routineNameEdit.getText().toString());
		
		dbAdapter.updateRoutine(routine);
	}
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}


}
