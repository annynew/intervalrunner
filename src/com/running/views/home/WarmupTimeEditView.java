package com.running.views.home;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * warmup time
 * @author amouradian
 *
 */
public class WarmupTimeEditView extends Activity  {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warmup_time_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        EditText warmupTimeEdit = (EditText) this.findViewById(R.id.editWarmupTimeId);
        warmupTimeEdit.setText(routine.getWarmupTime().toString());
    }

	@Override
	public void onPause() {
		super.onPause();
		EditText warmupTimeEdit = (EditText) this.findViewById(R.id.editWarmupTimeId);
		Routine routine = new Routine();
		routine.setId(routineId);
		Float value = (warmupTimeEdit.getText().toString() == null || warmupTimeEdit.getText().toString().trim().length()==0)?
				new Float(0):new Float(warmupTimeEdit.getText().toString());
		routine.setWarmupTime(value);
		
		dbAdapter.updateRoutine(routine);
	}
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}


}
