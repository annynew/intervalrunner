package com.running.views.home;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;

/**
 * Home dashboard class, entry point to app
 * @author amouradi
 *
 */
public class HomeDashboardView extends Activity implements OnClickListener{
    private DBRoutineAdapter db;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
       
     // create table if it does not exist
        db = new DBRoutineAdapter(this);

        // open a connection to the DB, and create
        // a table if one does not yet exist.
        db.open(); 
        ImageButton routineSettingsBtn = (ImageButton) this.findViewById(R.id.main_btn_routine_settings);
        routineSettingsBtn.setOnClickListener(this);
        
        ImageButton newRoutineBtn = (ImageButton) this.findViewById(R.id.main_btn_new_routine);
        newRoutineBtn.setOnClickListener(this);
        
        ImageButton runRoutineBtn = (ImageButton) this.findViewById(R.id.main_btn_run);
        runRoutineBtn.setOnClickListener(this);
        
       /* ImageButton playlistsBtn = (ImageButton) this.findViewById(R.id.main_btn_playlists);
        playlistsBtn.setOnClickListener(this);*/

    }
    
    @Override
    public void onClick(View v) {
        int buttonId = ((ImageButton)v).getId();
        
        if(buttonId == R.id.main_btn_routine_settings) {
            Intent intent = new Intent(v.getContext(), RoutineSettingsHomeView.class);
            startActivity(intent);
        }
        else if(buttonId == R.id.main_btn_new_routine) {
            long routineId = db.insertRoutineRecord();
            // pass new routineId to the next settings intent
            Bundle bundle = new Bundle();
            bundle.putLong(DBAdapterBase.ID, routineId);

            Intent intent = new Intent(v.getContext(), RoutineSettingsListView.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if(buttonId == R.id.main_btn_run) {
            Intent intent = new Intent(v.getContext(), RoutinesSelectHomeView.class);
            startActivity(intent); 
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }
    
}
