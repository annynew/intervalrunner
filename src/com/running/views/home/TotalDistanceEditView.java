package com.running.views.home;

import android.os.Bundle;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Routine;

/**
 * warmup time
 * @author amouradian
 *
 */
public class TotalDistanceEditView extends TextValidator {

	 private DBRoutineAdapter dbAdapter;
	 private long routineId;
	 private EditText totalDistanceEdit ;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.total_distance_layout);

        dbAdapter = new DBRoutineAdapter(this);
        dbAdapter.open();
        
        Bundle bundle = getIntent().getExtras();
        routineId  = bundle.getLong(DBAdapterBase.ID);
        
        //get routine name
        Routine routine = dbAdapter.getRoutineById(routineId);
        totalDistanceEdit = (EditText) this.findViewById(R.id.editTotalDistanceId);
        totalDistanceEdit.setText(routine.getTotalDistance().toString());
        totalDistanceEdit.addTextChangedListener(this);
    }

	@Override
	public void onPause() {
		super.onPause();
		
		Routine routine = new Routine();
		routine.setId(routineId);
		routine.setTotalDistance(replaceNullWithZero(totalDistanceEdit.getText().toString()));
		dbAdapter.updateRoutine(routine);
	}
	
	
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}

	@Override
	public void afterTextChanged(Editable editField) {
		
		TextView errText =  (TextView)this.findViewById(R.id.validate_not_null);
		validateNonNull(errText, editField.toString());
	}

}
