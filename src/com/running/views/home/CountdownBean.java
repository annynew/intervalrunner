package com.running.views.home;

public class CountdownBean {

////countdown properties
	private int secondsCounter = 0;
	private int elapsedTotalMinutesCounter = 0;
	private int elapsedTotalHoursCounter = 0;
	private int currentLapIndex = 0;
	private int elapsedLapSecsTotal = 0;

	private Float lapSeconds = 0f;
	private int lapSecondsCounter = 0;
	private int elapsedLapMins = 0;
	private int elapsedLapHours = 0;
	private long millisInFuture;
	private long previousMillis = 0;
	public int getSecondsCounter() {
		return secondsCounter;
	}
	public void setSecondsCounter(int secondsCounter) {
		this.secondsCounter = secondsCounter;
	}
	public int getElapsedTotalMinutesCounter() {
		return elapsedTotalMinutesCounter;
	}
	public void setElapsedTotalMinutesCounter(int elapsedTotalMinutesCounter) {
		this.elapsedTotalMinutesCounter = elapsedTotalMinutesCounter;
	}
	public int getElapsedTotalHoursCounter() {
		return elapsedTotalHoursCounter;
	}
	public void setElapsedTotalHoursCounter(int elapsedTotalHoursCounter) {
		this.elapsedTotalHoursCounter = elapsedTotalHoursCounter;
	}
	public int getCurrentLapIndex() {
		return currentLapIndex;
	}
	public void setCurrentLapIndex(int currentLapIndex) {
		this.currentLapIndex = currentLapIndex;
	}
	public int getElapsedLapSecsTotal() {
		return elapsedLapSecsTotal;
	}
	public void setElapsedLapSecsTotal(int elapsedLapSecsTotal) {
		this.elapsedLapSecsTotal = elapsedLapSecsTotal;
	}
	public Float getLapSeconds() {
		return lapSeconds;
	}
	public void setLapSeconds(Float lapSeconds) {
		this.lapSeconds = lapSeconds;
	}
	public int getLapSecondsCounter() {
		return lapSecondsCounter;
	}
	public void setLapSecondsCounter(int lapSecondsCounter) {
		this.lapSecondsCounter = lapSecondsCounter;
	}
	public int getElapsedLapMins() {
		return elapsedLapMins;
	}
	public void setElapsedLapMins(int elapsedLapMins) {
		this.elapsedLapMins = elapsedLapMins;
	}
	public int getElapsedLapHours() {
		return elapsedLapHours;
	}
	public void setElapsedLapHours(int elapsedLapHours) {
		this.elapsedLapHours = elapsedLapHours;
	}
	public long getMillisInFuture() {
		return millisInFuture;
	}
	public void setMillisInFuture(long millisInFuture) {
		this.millisInFuture = millisInFuture;
	}
	public long getPreviousMillis() {
		return previousMillis;
	}
	public void setPreviousMillis(long previousMillis) {
		this.previousMillis = previousMillis;
	}

	
	

}
