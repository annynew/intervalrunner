package com.running.views.home;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.running.R;
import com.running.adapters.ListViewAdapter;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.ListItem;
import com.running.model.Routine;

/**
 * Create routine here, list of options, where each will lead to its own screen
 * for that specific setting
 * 
 * @author amouradian
 * 
 */
public class RoutineSettingsListView extends ListActivity implements
		OnItemClickListener {

	private static final String MINUTES = " minutes";
	private List<ListItem> listItems;
	private DBRoutineAdapter dbAdapter;
	Routine routine;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbAdapter = new DBRoutineAdapter(this);
		dbAdapter.open();

		createList();

	}

	private void createList() {
		Bundle bundle = getIntent().getExtras();
		long routineId = bundle.getLong(DBAdapterBase.ID);

		// get routine name
		routine = dbAdapter.getRoutineById(routineId);

		populateListItems(bundle.getString(DBAdapterBase.NAME), routine,
				bundle.getIntegerArrayList("errItemIndexes"));
		setListAdapter(new ListViewAdapter(this, R.layout.custom_list_item, listItems));

		setContentView(R.layout.list_main);
		ListView myList = getListView();
		myList.setTextFilterEnabled(true);
		myList.setOnItemClickListener(this);

	}

	/* display a Toast with message text. */
	private void showMessage(CharSequence text) {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	/* this method is fired when an item is clicked */
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

		if (position == 0) {
			// show screen to enter routine name
			Intent intent = new Intent(v.getContext(), RoutineNameEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		}
		// warm up time
		else if (position == 1) {
			// show screen to enter total distance
			Intent intent = new Intent(v.getContext(), WarmupTimeEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		} else if (position == 2) {
			// total distance
			Intent intent = new Intent(v.getContext(), TotalDistanceEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		} else if (position == 3) {
			// sprint distance, preset values of 400/800/1200/1600 meters
			Intent intent = new Intent(v.getContext(), SprintDistanceEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		} else if (position == 4) {
			// regular pace
			Intent intent = new Intent(v.getContext(), RegularPaceEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		} else if (position == 5) {
			// sprint pace
			Intent intent = new Intent(v.getContext(), SprintPaceEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		} else if (position == 6) {
			// recovery time
			Intent intent = new Intent(v.getContext(), RecovertyTimeEditView.class);
			intent.putExtras(getIntent().getExtras());
			startActivity(intent);
		}
		// showMessage(((TextView)item.findViewById(R.id.toptext)).getText());
		dbAdapter.close();
	}

	private void populateListItems(String routineName, Routine routine,
			ArrayList<Integer> errItemIndexes) {

		listItems = new ArrayList<ListItem>();
		listItems.add(new ListItem("Routine Name", routine.getName()));
		String distUnit = (routine.isMetric()) ? " meters" : " miles";
		listItems.add(new ListItem("Warmup Time", routine.getWarmupTime() + MINUTES));
		
		if (errItemIndexes != null && errItemIndexes.contains(new Integer(2))) {
			ListItem totDistance = new ListItem("Total Distance", routine.getTotalDistance()
					.toString() + distUnit);
			totDistance.setColor("red");
			listItems.add(totDistance);
		} else {
			listItems.add(new ListItem("Total Distance", routine.getTotalDistance()
					.toString() + distUnit));
		}
		listItems.add(new ListItem("Sprint Distance", routine.getSprintDistance()
				.toString() + " meters"));

		if (errItemIndexes != null && errItemIndexes.contains(new Integer(4))) {
			ListItem regPace = new ListItem("Regular 5K Pace", routine.getRegularPace()
					.toString()
					+ " minutes /"
					+ distUnit.substring(0, distUnit.length() - 1));
			regPace.setColor("red");
			
			listItems.add(regPace);
		} else {
			listItems.add(new ListItem("Regular 5K Pace", routine.getRegularPace()
					.toString()
					+ " minutes /"
					+ distUnit.substring(0, distUnit.length() - 1)));
		}
		listItems.add(new ListItem("Sprint Pace", routine.getSprintPace().toString()
				+ " minutes /" + distUnit.substring(0, distUnit.length() - 1)));
		
		if (errItemIndexes != null && errItemIndexes.contains(new Integer(6))) {
			ListItem recTime = new ListItem("Time Between Sprints", routine.getRecoveryTime()
					.toString() + MINUTES);
			recTime.setColor("red");
			listItems.add(recTime);
		} else {
			listItems.add(new ListItem("Time Between Sprints", routine.getRecoveryTime()
					.toString() + MINUTES));
		}
	}

	/**
	 * Explicitly close our database connection when our application is done
	 * with it and we're about to quit.
	 */
	public void onDestroy() {
		super.onDestroy();
		dbAdapter.close();
	}

	public void onRestart() {
		super.onRestart();
		if (dbAdapter == null) {
			new DBRoutineAdapter(this);
			dbAdapter.open();
		} else if (!dbAdapter.isOpen()) {
			dbAdapter.open();
		}
		createList();
	}

}
