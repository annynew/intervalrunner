/**
 * 
 */
package com.running.views.home;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.running.R;
import com.running.db.DBAdapterBase;
import com.running.db.DBRoutineAdapter;
import com.running.model.Lap;
import com.running.model.Routine;
import com.running.model.Run;
import com.running.utils.TimeConverter;

/**
 * Preview your run screen, just display calculated run
 * 
 * @author amouradian
 * 
 */
public class PreviewRunView extends RoutinesBase implements OnClickListener{

	private DBRoutineAdapter db;
	private final Float mileInMeter = 0.000621371f;
	private final Float metersInMile = 1609.344001f;
	private List<Integer> errItemIndexes;

	Routine routine = new Routine();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// create table if it does not exist
		db = new DBRoutineAdapter(this);

		// open a connection to the DB, and create
		// a table if one does not yet exist.
		db.open();

		this.prepareScreen();
		
		

	}

	private boolean hasErrorValues(LayoutInflater inflater) {
		boolean hasErrorValues = false;
		LinearLayout errLayout = (LinearLayout)this.findViewById(R.id.preview_run_errors);
		
		TableLayout errValuesTable = (TableLayout)this.findViewById(R.id.preview_run_error_values);
		
		if(this.routine.getTotalDistance() == null || this.routine.getTotalDistance() == 0f){
			hasErrorValues = true;
			setErrorFieldValue(inflater, errValuesTable, getString(R.string.err_total_distance), routine.getTotalDistance().toString());
			errItemIndexes.add(new Integer(2));
		} 

		if(this.routine.getRegularPace() == null || this.routine.getRegularPace() == 0f){
			hasErrorValues = true;
			setErrorFieldValue(inflater, errValuesTable, getString(R.string.err_regular_pace), routine.getRegularPace().toString());
			errItemIndexes.add(new Integer(4));
		}
		
		if(this.routine.getRecoveryTime() == null || this.routine.getRecoveryTime() == 0f){
			hasErrorValues = true;
			setErrorFieldValue(inflater, errValuesTable, getString(R.string.err_recovery_time), routine.getRecoveryTime().toString());
			errItemIndexes.add(new Integer(6));
		}
		
		if(hasErrorValues) {
			errLayout.setVisibility(View.VISIBLE);
		} else{
			errLayout.setVisibility(View.GONE);
		}
		
		return hasErrorValues;
	}
	
	private void setErrorFieldValue(LayoutInflater inflater, TableLayout errValuesTable,  String errLabelText, String errValueText){
		TableRow errRow = (TableRow)inflater.inflate(R.layout.preview_run_error_field, errValuesTable, false);
		TextView errLabel = (TextView)errRow.findViewById(R.id.txt_error_label);
		TextView errValue = (TextView)errRow.findViewById(R.id.txt_error_value);
		errLabel.setText(errLabelText);
		errValue.setText(errValueText);
		errValuesTable.addView(errRow, errValuesTable.getChildCount());
	}
	
	private void prepareScreen() {
		
		setContentView(R.layout.preview_run);
		errItemIndexes = new ArrayList<Integer>();
		Bundle bundle = getIntent().getExtras();
		Long routineId = bundle.getLong(DBAdapterBase.ID);
		ImageButton runBtn = (ImageButton) this.findViewById(R.id.preview_run_runbtn);
		runBtn.setOnClickListener(this);
		
		float milesRunningTotal = 0f;
		LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout mainContent = (LinearLayout)this.findViewById(R.id.preview_run_main_content);
		
		// load routine from database
		this.routine = db.getRoutineById(routineId);
		
		TextView routineName = (TextView)this.findViewById(R.id.txt_routine_name);
		routineName.setText(this.routine.getName());
		
		if(hasErrorValues(inflater)){
			mainContent.setVisibility(View.INVISIBLE);
			return;
		}
		else{
			mainContent.setVisibility(View.VISIBLE);
		}
		//check if it has error data
		
		//fake routine data for now
		/*routine.setRecoveryTime(0.25f);
		routine.setRegularPace(3f);
		routine.setSprintDistance(200f);
		routine.setSprintPace(1f);
		routine.setTotalDistance(0.5f);
		routine.setWarmupTime(0.25f);*/
		
		// start calculating
		Run run = new Run();
		float totalTime = 0f;
		
		//add warmup lap
		Lap warmupLap = new Lap();
		warmupLap.setTitle("warmup");
		warmupLap.setLapMinutes(routine.getWarmupTime());
		warmupLap.setLapMiles(routine.getWarmupTime() * 1 / routine.getRegularPace());
		milesRunningTotal += warmupLap.getLapMiles();
		run.getListOfLaps().add(warmupLap);
		totalTime += warmupLap.getLapMinutes();
				
		boolean isSprint = true; //start with sprint after warmup
		
		float sprintMiles = routine.getSprintDistance()* mileInMeter;
		float sprintMinutes = routine.getSprintPace() * sprintMiles/ 1;
		float regularMiles = routine.getRecoveryTime()*1/routine.getRegularPace();
		
		int numOfSprints =0;
		
		//LayoutInflater inflater = getLayoutInflater();
		
		TableLayout table = (TableLayout)this.findViewById(R.id.preview_run_table);
		TableRow tr = (TableRow)inflater.inflate(R.layout.preview_run_tr_regular, table, false); 
		setTrContent(R.id.lap_title, warmupLap.getTitle(), tr);
		setTrContent(R.id.minutes, TimeConverter.convertMinsToTimeStr(warmupLap.getLapMinutes()), tr);
		setTrContent(R.id.miles, String.format("%.2f",warmupLap.getLapMiles()) + " miles", tr);
		setTrContent(R.id.miles_running_total, String.format("%.2f",milesRunningTotal) + " miles", tr);
		
		table.addView(tr, table.getChildCount());
		
		while (milesRunningTotal < routine.getTotalDistance()) {
			Lap lap = null;
			if(isSprint){
				//sprint minutes and distance
				milesRunningTotal = buildLap(milesRunningTotal, "sprint", sprintMinutes, sprintMiles, routine.getTotalDistance(),
						routine.getSprintPace(), run);
				lap = run.getListOfLaps().get(run.getListOfLaps().size()-1);
				tr = (TableRow)inflater.inflate(R.layout.preview_run_tr_sprint, table, false); 
				setTrContent(R.id.lap_title, lap.getTitle(), tr);
				setTrContent(R.id.minutes, TimeConverter.convertMinsToTimeStr(lap.getLapMinutes()), tr);
				Float sprintMeters = lap.getLapMiles()*metersInMile;
				setTrContent(R.id.miles, String.format("%.2f",sprintMeters) + " meters", tr);
				
				setTrContent(R.id.miles_running_total, String.format("%.2f",milesRunningTotal) + " miles", tr);
				
				View space = (View)inflater.inflate(R.layout.preview_run_tr_space, table, false); 
				table.addView(space, table.getChildCount());
				table.addView(tr, table.getChildCount());
				numOfSprints++;
			} else {
				milesRunningTotal = buildLap(milesRunningTotal, "regular", routine.getRecoveryTime(), regularMiles, 
						routine.getTotalDistance(),routine.getRegularPace(), run);
				lap = run.getListOfLaps().get(run.getListOfLaps().size()-1);
				tr = (TableRow)inflater.inflate(R.layout.preview_run_tr_regular, table, false); 
				setTrContent(R.id.lap_title, lap.getTitle(), tr);
				setTrContent(R.id.minutes, TimeConverter.convertMinsToTimeStr(lap.getLapMinutes()), tr);
				setTrContent(R.id.miles, String.format("%.2f",lap.getLapMiles()) + " miles", tr);
				setTrContent(R.id.miles_running_total, String.format("%.2f",milesRunningTotal) + " miles", tr);
				
				View space = (View)inflater.inflate(R.layout.preview_run_tr_space, table, false); 
				table.addView(space, table.getChildCount());
				table.addView(tr, table.getChildCount());
			}
			totalTime += lap.getLapMinutes();
			isSprint = !isSprint;
		}
		
		
		TextView txtTotalDist = (TextView)this.findViewById(R.id.txt_total_distance);
		txtTotalDist.setText(txtTotalDist.getText() + " " + String.format("%.2f",routine.getTotalDistance()));
		TextView txtTotalTime = (TextView)this.findViewById(R.id.txt_total_time);
		txtTotalTime.setText(txtTotalTime.getText() + " " +  TimeConverter.convertMinsToTimeStr(totalTime));
		TextView txtNumOfSprints = (TextView)this.findViewById(R.id.txt_total_num_of_sprints);
		txtNumOfSprints.setText(txtNumOfSprints.getText() + " " + String.valueOf(numOfSprints));
	}

	private void setTrContent(int elementId, String text, TableRow tr) {
		TextView lapTitle = (TextView)tr.findViewById(elementId);
		lapTitle.setText(text);
	}

	private float buildLap(float milesRunningTotal, String lapTitle, Float lapMinutes, Float lapDistance,
			Float totalDistance, Float lapPace, Run run){
		//sprint minutes and distance
		Lap lap = new Lap();
		lap.setTitle(lapTitle);
		
		if((milesRunningTotal + lapDistance) >= totalDistance ) {
			float milesLeft = totalDistance - milesRunningTotal;
			lap.setLapMiles(milesLeft);
			
			lap.setLapMinutes(milesLeft*lapPace);
			milesRunningTotal += milesLeft;
		} else {
			lap.setLapMinutes(lapMinutes);
			lap.setLapMiles(lapDistance);
			 milesRunningTotal += lapDistance;
		}
		run.getListOfLaps().add(lap);
		return milesRunningTotal;
	}
	
	
	/**
	 * Explicitly close our database connection when our application is done
	 * with it and we're about to quit.
	 */
	public void onDestroy() {
		super.onDestroy();
		db.close();
	}

    @Override
    public void onClick(View v) {
    	int buttonId = ((ImageButton)v).getId();
        Bundle bundle = new Bundle();
        bundle.putLong(DBRoutineAdapter.ID, routine.getId());
        bundle.putIntegerArrayList("errItemIndexes", (ArrayList<Integer>) this.errItemIndexes);

        if(buttonId == R.id.preview_run_runbtn){
	        Intent intent = new Intent(v.getContext(), RunCountDownView.class);
	        intent.putExtras(bundle);
	        startActivity(intent);
        } else if(buttonId == R.id.btn_edit_routine){

            Intent intent = new Intent(v.getContext(), RoutineSettingsListView.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void onRestart() {
        super.onRestart();
        if(db == null) {
        	new DBRoutineAdapter(this);
        	db.open();
        }
        prepareScreen();
    }
}
