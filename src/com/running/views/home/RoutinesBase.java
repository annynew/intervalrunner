package com.running.views.home;

import com.running.R;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public abstract class RoutinesBase extends Activity {

	  public boolean onCreateOptionsMenu(Menu menu) {

	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.options_menu, menu);
	        return true;
	    }

	    public boolean onOptionsItemSelected(MenuItem item) {
	        // go to Settings or Routines screen screen
	        if (item.getItemId() == R.id.routines) {
	        	Intent intent = new Intent(this, RoutineSettingsHomeView.class);
	            startActivity(intent);
	        } 
	        else if(item.getItemId() == R.id.home) {
	        	Intent intent = new Intent(this, HomeDashboardView.class);
	            startActivity(intent);
	        }

	        return true;
	    }
}
